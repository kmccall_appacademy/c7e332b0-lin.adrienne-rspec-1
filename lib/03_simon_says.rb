def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, count = 2)
  Array.new(count, string).join(' ')
end

def start_of_word(string, count)
  string[0...count]
end

def first_word(str)
  str.split.first
end

def titleize(str)
  little_words = %w[a the and of over]
  result_arr = str.split.map.with_index do |word, i|
    if !i.zero? && little_words.include?(word)
      word.downcase
    else
      word.capitalize
    end
  end
  result_arr.join(' ')
end
