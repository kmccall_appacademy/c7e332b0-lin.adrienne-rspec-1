def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(arr)
  return 0 if arr == []
  arr.reduce(:+)
end

def multiply(*nums)
  nums.reduce(:*)
end

def power(num1, num2)
  num1**num2
end

def factorial(num)
  return 1 if num <2
  num *= factorial(num-1)
end
