def translate(str)
  result_arr = str.split.map do |word|
    result_word = ''
    vowel_i = vowel_index(word)
    if vowel_i.zero?
      result_word = word + 'ay'
    else
      result_word = word[vowel_i..-1] + word[0...vowel_i] + 'ay'
    end

    result_word
  end
  result_arr.join(' ')
end

private

def vowel_index(word)
  vowels = 'aeiou'
  word.each_char.with_index do |ch, i|
    return (word.index('qu') + 2) if word.include?('qu')
    return i if vowels.include?(ch)
  end
end
